package com.xudongbase.common.util.date;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class CustomDateTest {

    @Test
    public void testDateStrToLocalDateTime(){
        log.info(CustomDateUtil.toLocalDateTime("2023-08-18").toString());
        log.info(CustomDateUtil.toLocalDateTimeByDayStart("2023-08-18").toString());
        log.info(CustomDateUtil.toLocalDateTimeByDayEnd("2023-08-18").toString());
    }

}
