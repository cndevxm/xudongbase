package com.xudongbase.java;

import com.xudongbase.common.util.EqualsUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
public class EqualsTest {
    @Test
    public void testEquals() {
        Integer num1 = null;
        Integer num2 = 2;
        Double num3 = null;
        Double num4 = 3d;
        BigDecimal num5 = null;
        BigDecimal num6 = BigDecimal.ZERO;
        Long num7 = null;
        Long num8 = 4l;
        Float num9 = 4.0f;
        Float num10 = 4f;
        System.out.println(EqualsUtil.equals(num1, num2));
        System.out.println(EqualsUtil.equals(num3, num4));
        System.out.println(EqualsUtil.equals(num5, num6));
        System.out.println(EqualsUtil.equals(num7, num8));
        System.out.println(EqualsUtil.equals(num9, num10));
    }
}
