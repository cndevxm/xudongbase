package com.xudongbase.java;

import com.xudongbase.common.constant.FormatConstant;
import com.xudongbase.common.util.DefaultUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class DefaultTest {
    @Test
    public void testDefault() {
        String str1 = "";
        String str2 = "null";
        String str3 = "NULL";
        Double num1 = null;
        Double num2 = 1d;
        Date date1 = null;
        Date date2 = new Date();
        System.out.println(DefaultUtil.getStringValue(str1, "#"));
        System.out.println(DefaultUtil.getStringValue(str2, "#"));
        System.out.println(DefaultUtil.getStringValue(str2, "#", true));
        System.out.println(DefaultUtil.getStringValue(str3, "#", true));
        System.out.println(DefaultUtil.getValue(num1, "0"));
        System.out.println(DefaultUtil.getValue(num2, "0"));
        System.out.println(DefaultUtil.getValue(date1, "0"));
        System.out.println(DefaultUtil.getStringValue(date1, FormatConstant.DATE_YMD, "-"));
        System.out.println(DefaultUtil.getStringValue(date2, FormatConstant.DATE_YMD, "-"));
        System.out.println(DefaultUtil.getStringValue(date2, FormatConstant.DATE_YMDHMS, "-"));
        System.out.println(DefaultUtil.getStringValue(date2, "", "-"));
    }
}
