package com.xudongbase.json;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xudongbase.common.model.TestModel;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class GsonSerializeTest {


    /**
     * 测试单个实体反序列化和反序列化
     */
    @Test
    public void testModelSerializeAndDeserialize() {
        TestModel testModel = new TestModel();
        testModel.setLocalDate(LocalDate.now());
        testModel.setLocalDateTime(LocalDateTime.now());
        Gson gson = new Gson();
        String jsonStr = gson.toJson(testModel);
        testModel = gson.fromJson(jsonStr, TestModel.class);
    }

    /**
     * 测试集合反序列化和反序列化
     */
    @Test
    public void testCollectionSerializeAndDeserialize() {
        List<TestModel> testModelList = new ArrayList<>();
        TestModel testModel = new TestModel();
        testModel.setLocalDate(LocalDate.now());
        testModel.setLocalDateTime(LocalDateTime.now());
        testModelList.add(testModel);
        Gson gson = new Gson();
        String jsonStr = gson.toJson(testModelList);
        testModelList = gson.fromJson(jsonStr, new TypeToken<List<TestModel>>() {
        }.getType());
    }
}
