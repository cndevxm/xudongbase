package com.xudongbase.json;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xudongbase.common.model.Result;
import com.xudongbase.common.model.TestModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class JsonSerializeTest {
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 测试fastjson，long、Date、LocalDateTime类型序列化
     */
    @Test
    public void testFastjsonSerialize() {
        List<Object> list = new ArrayList<>();
        list.add(1440931124753108994L);
        list.add(new Date());
        list.add(LocalDate.now());
        list.add(LocalDateTime.now());
        System.out.println(JSONObject.toJSONString(list));
    }

    /**
     * 测试LocalDate类反序列化
     */
    @Test
    public void testLocalDateDeserialize() {
        List<TestModel> testModelList = new ArrayList<>();
        TestModel testModel = new TestModel();
        testModel.setLocalDate(LocalDate.now());
        testModel.setLocalDateTime(LocalDateTime.now());
        testModelList.add(testModel);
        Gson gson = new Gson();
        String jsonStr = gson.toJson(testModelList);
//        gson.fromJson(jsonStr, TestModel.class);
        testModelList = gson.fromJson(jsonStr, new TypeToken<List<TestModel>>() {
        }.getType());
//        List<TestModel> testModelList = new ArrayList<>();
//        TestModel testModel = new TestModel();
//        testModel.setLocalDateStr(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
//        testModel.setLocalDateTimeStr(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
//        testModelList.add(testModel);
//        Gson gson = new Gson();
//        String jsonStr = gson.toJson(testModelList);
//        testModelList = JSONObject.parseArray(jsonStr, TestModel.class);
    }
}
