package com.xudongbase.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.xudongbase.common.model.Result;
import com.xudongbase.common.model.TestModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class JacksonSerializeTest {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 测试jackson，long、Date、LocalDateTime类型序列化
     */
    @Test
    public void testJacksonSerialize() throws Exception {
        List<Object> list = new ArrayList<>();
        list.add(1440931124753108994L);
        list.add(new Date());
        list.add(LocalDate.now());
        list.add(LocalDateTime.now());
        System.out.println(objectMapper.writeValueAsString(list));
    }

    /**
     * 测试jackson，反序列化
     */
    @Test
    public void testJacksonDeserialize() throws Exception {
        List<Result> list = new ArrayList<>();
        list.add(Result.error(null, 1440931124753108994L));
        list.add(Result.error(null, new Date()));
        list.add(Result.error(null, LocalDate.now()));
        list.add(Result.error(null, LocalDateTime.now()));
        String listStr = objectMapper.writeValueAsString(list);
        list = objectMapper.readValues(objectMapper.getFactory().createParser(listStr), new TypeReference<List<Result>>() {
        }).readAll().get(0);
    }

    /**
     * 测试单个实体反序列化和反序列化
     */
    @Test
    public void testModelSerializeAndDeserialize() throws Exception {
        TestModel testModel = new TestModel();
        testModel.setLocalDate(LocalDate.now());
        testModel.setLocalDateTime(LocalDateTime.now());
        String jsonStr = objectMapper.writeValueAsString(testModel);
        testModel = objectMapper.readValue(objectMapper.getFactory().createParser(jsonStr),TestModel.class);
    }

    /**
     * 测试集合反序列化和反序列化
     */
    @Test
    public void testCollectionSerializeAndDeserialize() throws Exception  {
        List<TestModel> testModelList = new ArrayList<>();
        TestModel testModel = new TestModel();
        testModel.setLocalDate(LocalDate.now());
        testModel.setLocalDateTime(LocalDateTime.now());
        testModelList.add(testModel);
        String jsonStr = objectMapper.writeValueAsString(testModelList);
        testModelList = objectMapper.readValues(objectMapper.getFactory().createParser(jsonStr), new TypeReference<List<TestModel>>() {
        }).readAll().get(0);
    }
}
