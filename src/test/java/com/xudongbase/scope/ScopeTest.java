package com.xudongbase.scope;

import com.xudongbase.common.util.ScopeUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@SpringBootTest
public class ScopeTest {
    @Test
    public void testScope() {
        LocalDate startDate1 = LocalDate.parse("2022-05-01", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate1 = LocalDate.parse("2022-05-10", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate startDate2 = LocalDate.parse("2022-05-05", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endDate2 = LocalDate.parse("2022-05-05", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        if (ScopeUtil.isIntersection(startDate1, endDate1, startDate2, endDate2)) {
            System.out.println("true");
        }
        if (ScopeUtil.isIntersection(startDate2, endDate2, startDate1, endDate1)) {
            System.out.println("true");
        }
    }
}
