package com.xudongbase.stream;

import cn.hutool.core.util.StrUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
public class StreamTest {
    @Test
    public void testGroupBy() {
        List<String> strList = new ArrayList<>(Arrays.asList("11", "12", "13", null, null));
        Map<String, List<String>> map = strList.stream().collect(Collectors.groupingBy(x -> StrUtil.isEmpty(x) ? "" : x));
        LocalDate.parse("2022-03-10", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }
}
