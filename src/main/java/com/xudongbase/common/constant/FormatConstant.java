package com.xudongbase.common.constant;

/**
 * 格式常量类
 *
 * @author xudongmaster
 */
public class FormatConstant {
    /**
     * 日期格式（年月日）
     */
    public static String DATE_YMD = "yyyy-MM-dd";
    /**
     * 日期格式（年月日时分秒）
     */
    public static String DATE_YMDHMS = "yyyy-MM-dd HH:mm:ss";
}
