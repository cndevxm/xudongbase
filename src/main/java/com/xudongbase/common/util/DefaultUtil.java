package com.xudongbase.common.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.xudongbase.common.constant.FormatConstant;

import java.util.Date;

/**
 * 默认值工具类
 *
 * @author xudongmaster
 */
public class DefaultUtil {
    /**
     * 获取值，值为null时使用默认值
     *
     * @param displayValue 显示值
     * @param defaultValue 默认值
     * @return 值
     */
    public static <T> T getValue(T displayValue, T defaultValue) {
        if (displayValue == null) {
            return defaultValue;
        }
        return displayValue;
    }

    /**
     * 获取值，值为null或空字符时使用默认值
     *
     * @param displayValue 显示值
     * @param defaultValue 默认值
     * @return 字符串值
     */
    public static String getStringValue(String displayValue, String defaultValue) {
        return getStringValue(displayValue, defaultValue, false);
    }

    /**
     * 获取值，值为null或空字符时使用默认值
     *
     * @param displayValue      显示值
     * @param defaultValue      默认值
     * @param filterNullStrFlag 过滤null字符串标记
     * @return 字符串值
     */
    public static String getStringValue(String displayValue, String defaultValue, boolean filterNullStrFlag) {
        if (StrUtil.isEmpty(displayValue)) {
            return defaultValue;
        }
        //过滤null字符串标记为true，并且显示值为null字符串，则返回默认值
        if (filterNullStrFlag && StrUtil.equals(displayValue, "null", true)) {
            return defaultValue;
        }
        return displayValue;
    }

    /**
     * 获取值，值为null或空字符时使用默认值
     *
     * @param displayValue 显示值
     * @param format       显示格式
     * @param defaultValue 默认值
     * @return 字符串值
     */
    public static String getStringValue(Date displayValue, String format, String defaultValue) {
        if (displayValue == null) {
            return defaultValue;
        }
        //日期显示格式为空时，默认使用年月日时分秒格式
        if (StrUtil.isEmpty(format)) {
            format = FormatConstant.DATE_YMDHMS;
        }
        return DateUtil.format(displayValue, format);
    }
}
