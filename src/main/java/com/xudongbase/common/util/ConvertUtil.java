package com.xudongbase.common.util;

import com.google.common.primitives.Bytes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 转换工具类
 *
 * @author xudongmaster
 */
public class ConvertUtil {
    /**
     * byte数组转List
     *
     * @param bytes byte数组
     * @return List
     */
    public static List<Byte> arrayToList(byte[] bytes) {
        return new ArrayList<>(Bytes.asList(bytes));
    }

    /**
     * Byte数组转List
     *
     * @param bytes Byte数组
     * @return List
     */
    public static List<Byte> arrayToList(Byte[] bytes) {
        return new ArrayList<>(Arrays.asList(bytes));
    }

    /**
     * Integer数组转List
     *
     * @param array Integer数组
     * @return List
     */
    public static List<Integer> arrayToList(Integer[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    /**
     * Long数组转List
     *
     * @param array Long数组
     * @return List
     */
    public static List<Long> arrayToList(Long[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    /**
     * Double数组转List
     *
     * @param array Double数组
     * @return List
     */
    public static List<Double> arrayToList(Double[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }

    /**
     * Float数组转List
     *
     * @param array Float数组
     * @return List
     */
    public static List<Float> arrayToList(Float[] array) {
        return new ArrayList<>(Arrays.asList(array));
    }
}
