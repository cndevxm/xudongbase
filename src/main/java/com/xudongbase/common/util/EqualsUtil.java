package com.xudongbase.common.util;

/**
 * 相等工具类
 *
 * @author xudongmaster
 */
public class EqualsUtil {
    /**
     * 判断相等
     *
     * @param num1 对象1
     * @param num2 对象2
     * @return true相等，false不相等
     */
    public static boolean equals(Object num1, Object num2) {
        if (num1 == null && num2 == null) {
            return true;
        }
        if (num1 == null || num2 == null) {
            return false;
        }
        //==比较比equals比较性能更好
        if (num1 == num2) {
            return true;
        }
        return num1.equals(num2);
    }
}
