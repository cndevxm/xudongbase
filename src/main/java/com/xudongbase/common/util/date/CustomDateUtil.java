package com.xudongbase.common.util.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 自定义日期工具类
 *
 * @author xudongmaster
 */
public class CustomDateUtil {

    /**
     * 日期字符串转LocalDateTime
     *
     * @param dateStr 日期字符串yyyy-MM-dd
     * @return LocalDateTime
     */
    public static LocalDateTime toLocalDateTime(String dateStr) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay();
    }

    /**
     * 日期字符串转LocalDateTime(当前天00:00:00)
     *
     * @param dateStr 日期字符串yyyy-MM-dd
     * @return LocalDateTime
     */
    public static LocalDateTime toLocalDateTimeByDayStart(String dateStr) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay();
    }

    /**
     * 日期字符串转LocalDateTime(当前天59:59:59)
     *
     * @param dateStr 日期字符串yyyy-MM-dd
     * @return LocalDateTime
     */
    public static LocalDateTime toLocalDateTimeByDayEnd(String dateStr) {
        return LocalDate.parse(dateStr, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
                .plusDays(1).atStartOfDay()
                .minusSeconds(1);
    }
}
