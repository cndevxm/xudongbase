package com.xudongbase.common.exception;

import com.xudongbase.common.model.Result;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 捕获全局异常统一处理器
 *
 * @author xudongmaster
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 通用异常处理
     *
     * @param request   请求
     * @param exception 异常
     * @return 结果
     */
    @ExceptionHandler({Exception.class})
    public Result exceptionHandler(HttpServletRequest request, Exception exception) {
        exception.printStackTrace();
        return Result.error(exception.getMessage());
    }
}