package com.xudongbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XuDongBaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(XuDongBaseApplication.class, args);
    }

}
