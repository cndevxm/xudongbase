package com.xudongbase.test.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
    @Async
    public void test(int num) {
        System.out.print("\tnum=" + num);
    }
}
